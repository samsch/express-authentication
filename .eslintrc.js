'use strict';

module.exports = {
	root: true,
	extends: '@samsch/eslint-config-default',
	rules: {
		'no-console': ['error', { allow: ['error'] }],
		'no-warning-comments': ['error', { terms: ['todo', 'todo:'] }],
		'react/prop-types': ['off'],
	},
};
