'use strict';

const config = require('./config.json');

module.exports = {
	client: 'pg',
	connection: {
		...config.knex,
	},
	migrations: {
		stub: require('@samsch/smart-migrations').getDefaultStubPath(),
	},
};
