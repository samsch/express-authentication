'use strict';

const { migrator, types } = require('@samsch/smart-migrations');

module.exports = migrator([
	{
		tables: [],
		up: async knex => {
			await knex.raw(`
CREATE FUNCTION raise_exception() RETURNS trigger AS $$
BEGIN
	RAISE EXCEPTION 'May not update created_at timestamps - on table %', TG_TABLE_NAME;
END;
$$ LANGUAGE plpgsql;`);
		},
		down: async knex => {
			await knex.raw('DROP FUNCTION raise_exception;');
		},
	},
	{
		tables: 'users',
		easyTable: {
			id: types.id_int,
			username: 'text',
			password: 'text',
			name: 'text',
			...types.timestamps(),
		},
	},
	{
		tables: 'sessions',
		easyTable: {
			sid: 'text|primary',
			sess: 'json',
			expired: 'timestamp|index',
		},
	},
]);
