# Example Express Project with Authentication

## TODO List

- Setup some kind of basic general form validation
- Do anything in various TODOs throughout the app
- Write documentation
	- Database handling
	- Feature lists
	- Security trade-offs
	- Server side rendering notes
	- Working with mixed server and client side rendering
	- Explanation of parametric modules for deps
- login.js/logout.js should maybe be part of the autht service.
- Fix Login and Registration display of already logged in view
- Consider basic shared Navigation in page rendering
- Add user update functionality and form
- Make sure various errors in the app are logged usefully for at least dev
- Write explanation for migration-util/reference.js
- Form errors need to have correct aria-described-by handling for errors.
- Form error display should also be better in general. Maybe an error list with links to the fields.
- Need to figure out how to minimally give an example for styling server side pages.
- Need to add client side demo for json-authentication and a basic view.
- Write out more TODOs
- Get this reviewed

### Possible additions

- Figure out how to document handling password reset via email. Also maybe email-based login?
- Would be nice to have an example of adding app functionality routes

## List of parts of the app which are not necessary, and exist as basic examples

- You only need either the `src/authentication/routes.js` and `views/authentication` OR `src/authentication/json-routes.js`. Use the one that reflects how you login pages, registration flow, and profile pages will work. If you remove one, don't forget to also remove the require call and `router.use()` call for it in `src/routes.js`. You could also mix and match, or even use both. Fully server rendered login and registration pages can be more secure, if you also block via CSP any JavaScript from executing on them.
- Comprehensive input validation for json authentication routes. You can do that entirely on the front end, and do a basic existence check and return a plain 400.
- All server rendering isn't strictly necessary. I would *recommend* server rendering your base html page for your front end, as that lets you include authentication/user state (and possibly other useful data) in the initial page load. There are tradeoff which you might want to consider either way. If you choose to do no server side rendering, the setup in `src/main.js` can be removed. You could also replace the jsx Inferno engine with a different choice.
- Error handling does not need to be as comprehensive in some places. You could just catch any error and return a 500. You mostly just make your job harder by doing that though.
- Username (or email) plus password login isn't required, if you replace it with some other method of confirming user identities, such as third party authentication. However, for the purposes of privacy, username + password is the best case. For making it easier for user to recover from forgotten passwords, it would be a good idea to have them give and to store an email address (or other contact method) on the user which can be used for password resets.
- The dependencies handling is setup to scale reasonably well with application complexity. It could be replaced with a simpler implementation if your app doesn't need those features. Making this work smoothly and without hard to predict bugs takes some skill and experience though.
- The postgres testing script `scripts/postgres/pg-create-testing-db.sql` is not necessary for the app, but gives a nice place where you can run tests against generated data. In local dev, it's also practically free to setup in the Docker DB container, so you don't gain much by removing it.
- `scripts/make-config.js` is purely a convenience. You can build config.json by hand, or use external sources for it. A secret store comes to mind as being very reasonable.
- The database is setup with PostgreSQL. That could be replaced with any suitable database. Don't forget to switch out the sessions store connector to match though. This also would possibly mean removing or replacing `migrations` and `migrations-util` folders and contents.
- The `docker-compose.yml` file (and .gitignore of `databasevolume`) can be removed or replaced if you want to use another source of database.
- Strictly speaking, `src/authentication/require-auth-middleware.js` could be combined into `src/global-middleware/authentication.js` and setup to only run before the profile server side authentication route and change password routes. Be careful with how that affects error pages though, that can become a mess if not handled correctly. I DO NOT recommend this change.
- `src/models` is not necessary, nor the usage of it in `src/dependencies.js`. Instead, you could use knex or whatever other database access tools directly, or setup a different system. This is just one nice way to organize the database sourced entities you work with. Keeping all the access to something like the user in one place makes it easy to make changes.
- Hopefully it should be obvious that the meta dev files like `.eslintrc.js` and `prettier.config.js` can be changed to suit. You will probably also want to replace the license with your own. I am not a lawyer, but you might want to include a note and link to this project that yours derives from it.
- The entire organization of the app itself. This is only one way the files can be arranged, and the functionality split up. Check out the [note on making other changes](#Note_on_making_other_changes) below too though.
- There are various places where functionality is split into multiple files, where it at least at the surface level seems like it could be one file. Some of this is forward-looking organization (almost no app will *only* have a user model), some of it is for the purpose of making the app easier to understand. Some of those decisions may be subjective. Per the [note on making other changes](#Note_on_making_other_changes) below, be careful making changes, but also feel free to streamline something that makes your app easier to understand.

### Note on making other changes

Before removing or modifying other parts of the application, you must take the time to understand what they do. A lot of the code (especially the rest not noted above) is related at least tangentially to necessary security and privacy functionality. Functional and safe authentication is *hard*. Do not feel embarrassed if you need to ask for help. One good place to ask questions would be the Reactiflux Discord server's #help-backend channel. If it's not busy, and you see me (user samsch#1439), feel free to ping to let me know you are using this example! I can't promise to be available to answer questions, but I love to hear from users.

## Custom Errors

This app creates custom errors by creating a class that extends `Error`. This *will not* work if you transpile it to ES5 or older with certain tools. As this is meant to run in Node.js, and primarily focused on new projects, it is extremely unlikely that support for Node.js older than version 6 is necessary.

More information on the possible transpiling problems here though:
- https://stackoverflow.com/questions/1382107/whats-a-good-way-to-extend-error-in-javascript
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error#es6_custom_error_class
