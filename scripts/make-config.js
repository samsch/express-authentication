'use strict';

/**
 * This script generates the development application config file.
 * We are using the default PostgreSQL database, user, and port,
 * and assuming that the database is available on localhost, and
 * that the password is "postgres". Change this as needed to your
 * default development config, such as what you might have in a
 * docker-compose.yml file, or vagrant setup.
 *
 * You could add any other default configuration here, such as
 * api keys that can be retrieved via a local connection (might
 * need to change the flow of the file for async handling).
 */

const fs = require('fs');
const { nanoid } = require('nanoid');
const path = require('path');

const config = {
	env: 'development',
	sessionSecret: nanoid(),
	port: 8000,
	knex: {
		host: 'localhost',
		user: 'postgres',
		password: 'postgres',
		database: 'postgres',
		port: 5432,
	},
};

// As this is a single run script, the synchronous fs function is fine.
// Remember not to use sync functions in your actual app!
fs.writeFileSync(
	path.resolve(__dirname, '../config.json'),
	JSON.stringify(config, null, '\t'),
);
