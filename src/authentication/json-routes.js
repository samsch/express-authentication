'use strict';

const Promise = require('bluebird');
const PromiseRouter = require('express-promise-router');
const FormError = require('../errors/FormError');
const IncorrectCredentials = require('./errors/IncorrectCredentials');
const logout = require('./logout');

const router = PromiseRouter();

module.exports = function makeRouter({ services }) {
	router.post('/register', (req, res) => {
		const { username, password, name } = req.body;
		// TODO validate input
		return Promise.try(() => {
			return services.authentication.register(req, {
				username,
				password,
				name,
			});
		}).then(() => {
			res.json({
				message: `Registered as ${req.user.name}`,
			});
		});
	});
	router.post('/login', async (req, res) => {
		return Promise.try(() => {
			const errors = {};
			if (!req.body.username) {
				errors.username = ['Required'];
			}
			if (!req.body.password) {
				errors.password = ['Required'];
			}
			if (errors.username || errors.password) {
				throw new FormError(errors);
			}
			return Promise.try(() => {
				return services.authentication.login(req, {
					username: req.body.username,
					password: req.body.password,
				});
			}).then(() => {
				res.json({
					message: 'Logged in!',
				});
			});
		})
			.catch(IncorrectCredentials, () => {
				res.status(401).json({
					message: 'Incorrect username or password',
				});
			})
			.catch(FormError, formError => {
				res.status(400).json({
					formErrors: formError.errorsMap,
				});
			});
	});

	router.post('/logout', (req, res) => {
		return Promise.try(() => {
			return logout(req);
		}).then(() => {
			res.json({
				message: 'You have logged out!',
			});
		});
	});

	return router;
};
