'use strict';

/**
 * This is the entire functionalty that sets a user as logged in.
 */

const regenerateSession = require('./regenerate-session');

module.exports = async function login(req, user) {
	return regenerateSession(req).then(() => {
		req.session.user_id = user.id;
		// Hint:
		// Would be very reasonable to have audit functionality here, such as
		// writing `now()` to a "last_login" column on the user, or logging to a
		// security events table.

		// In other routes, this is done by the global authentication middleware
		req.user = user;
	});
};
