'use strict';

const regenerateSession = require('./regenerate-session');

module.exports = async function login(req) {
	return regenerateSession(req);
};
