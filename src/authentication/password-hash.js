'use strict';

/**
 * If you wanted to switch out the password hashing library, this is where you
 * would do that. Make sure that the verify return value is the same on the new
 * library before dropping it in. Some libraries throw when the password
 * doesn't match, while some like this one (argon2) resolve to a true or false
 * value.
 *
 * What we want is to throw InvalidPassword if it doesn't match, and resolve
 * `undefined` if it does. This is better than resolving to true or false, as
 * it is a more "fail-closed" system, where if a mistake in usage is made, it
 * should be less likely to be bugged in a way that allows users in when they
 * should not be allowed.
 *
 * argon2 is currently the general top choice for password hashing algorithms.
 * Within the Node ecosystem, only argon2 and bcrypt currently have
 * implementations right now which are safe to use. There are Node bindings for
 * scrypt, but it unfortunately is not exposed in a way that is safe to use, as
 * it doesn't default to the correct usage for password hashing.
 */

const argon2 = require('argon2');
const InvalidPassword = require('./errors/InvalidPassword');

module.exports.hash = async function hash(raw) {
	return argon2.hash(raw, { type: argon2.argon2id });
};

module.exports.verify = async function verify(hash, raw) {
	return argon2.verify(hash, raw).then(result => {
		if (!result) {
			throw new InvalidPassword();
		}
	});
};
