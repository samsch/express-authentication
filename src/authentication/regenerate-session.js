'use strict';

/**
 * This exists as it's own module for two reasons. One, promisifying the
 * function in one place. And two, re-attaching the csrf token in one place.
 */

const Promise = require('bluebird');

module.exports = function regenerateSession(req) {
	return Promise.fromCallback(callback => {
		req.session.regenerate(callback);
	}).then(() => {
		req.attachCSRFCookie();
	});
};
