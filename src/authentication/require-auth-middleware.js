'use strict';

const HttpError = require('../errors/HttpError');

function urlToFullPathAndQuery(url) {
	const parts = new URL(url, 'http://base');
	return parts.pathname + parts.search;
}

module.exports = function requireAuthMiddlewareFactory(deniedStatus = 404) {
	return function requireAuthMiddleware(req, res, next) {
		if (!req.user) {
			if (deniedStatus === 'login') {
				res.redirect(
					303,
					`/user/login?redirect=${urlToFullPathAndQuery(req.originalUrl)}`,
				);
			} else {
				throw HttpError(deniedStatus);
			}
		}
		next();
	};
};
