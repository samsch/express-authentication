'use strict';

const Promise = require('bluebird');
const PromiseRouter = require('express-promise-router');
const FormError = require('../errors/FormError');
const IncorrectCredentials = require('./errors/IncorrectCredentials');
const logout = require('./logout');
const requireAuthMiddleware = require('./require-auth-middleware');

const router = PromiseRouter();

// TODO related to validating input
function requiredString(value) {
	const valid = typeof value === 'string' && value.length > 0;
	return valid ? [] : ['Required'];
}

module.exports = function makeRouter({ services }) {
	router.get('/register', (req, res) => {
		res.render('authentication/register.jsx');
	});
	router.post('/register', (req, res) => {
		const { username, password, name } = req.body;
		// TODO validate input
		return Promise.try(() => {
			return services.authentication.register(req, {
				username,
				password,
				name,
			});
		})
			.then(() => {
				req.flash('message', `Registered as ${req.user.name}`);
				res.redirect(303, '/user/profile');
			})
			.catch(FormError, formError => {
				req.flash('form-errors', formError.errorsMap);
				res.redirect(303, '');
			});
	});

	router.get('/login', (req, res) => {
		res.render('authentication/login.jsx');
	});
	router.post('/login', async (req, res) => {
		return Promise.try(() => {
			const errors = {};
			if (!req.body.username) {
				errors.username = ['Required'];
			}
			if (!req.body.password) {
				errors.password = ['Required'];
			}
			if (errors.username || errors.password) {
				throw new FormError(errors);
			}
			return Promise.try(() => {
				return services.authentication.login(req, {
					username: req.body.username,
					password: req.body.password,
				});
			}).then(() => {
				req.flash('message', 'Logged in!');
				res.redirect('/user/profile');
			});
		})
			.catch(IncorrectCredentials, () => {
				req.flash('form-message', 'Incorrect username or password');
				res.redirect(303, '');
			})
			.catch(FormError, formError => {
				req.flash('form-errors', formError.errorsMap);
				res.redirect(303, '');
			});
	});

	router.get('/profile', requireAuthMiddleware('login'), (req, res) => {
		res.render('authentication/profile.jsx');
	});

	router.post('/logout', (req, res) => {
		return Promise.try(() => {
			return logout(req);
		}).then(() => {
			req.flash('message', 'You have logged out!');
			res.redirect(303, '/user/login');
		});
	});

	return router;
};
