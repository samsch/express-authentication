'use strict';

const Promise = require('bluebird');
const NotFoundError = require('../errors/NotFoundError');
const IncorrectCredentials = require('./errors/IncorrectCredentials');
const passwordHash = require('./password-hash');
const loginUser = require('./login');
const InvalidPassword = require('./errors/InvalidPassword');

module.exports = function makeAuthenticationService({ models }) {
	async function register(req, { username, password, name }) {
		return Promise.try(() => {
			return passwordHash.hash(password);
		})
			.then(hash => {
				return models.user.createUser(
					username,
					hash,
					name === '' ? username : name,
				);
			})
			.then(user => {
				return loginUser(req, user);
			});
	}

	async function login(req, { username, password }) {
		return Promise.try(() => {
			// Hint:
			// If you have a user ban feature, you could add a check for that here,
			// and return a not found result. Similarly, you could check for any
			// other reasons you might want a user to not be able to log in.
			// Note:
			// This login flow is not safe from username enumeration. Because it
			// fails quickly when the username is not found, a user can probably
			// figure out the difference between a username existing or not.
			// Since this app is based on unique username logins though, that
			// enumeration can't be avoided, as your username has to be unique,
			// and a duplicate would give an error at the registration screen.
			return models.user.findLoginUser(username);
		})
			.then(user => {
				return Promise.try(() => {
					return passwordHash.verify(user.password, password);
				}).then(() => {
					return models.user.findUserById(user.id);
				});
			})
			.then(user => {
				return loginUser(req, user);
			})
			.catch(NotFoundError, InvalidPassword, () => {
				throw new IncorrectCredentials();
			});
	}

	async function changePassword(req, newRaw, oldRaw) {
		return Promise.try(() => {
			return models.user.findLoginUser(req.user.username);
		})
			.then(({ password }) => {
				return passwordHash.verify(password, oldRaw);
			})
			.then(() => {
				return passwordHash.hash(newRaw);
			})
			.then(hash => {
				return models.user.setPassword(req.user.id, hash);
			});
	}

	return {
		register,
		login,
		changePassword,
	};
};
