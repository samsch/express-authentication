'use strict';

const assert = require('assert').strict;
const config = require('../config.json');

// TODO Setup validation via validatem for config.json, and on error, remind user to generate the file.
// config validity checks.
assert(
	config.sessionSecret,
	'config.sessionSecret must be set to long random string',
);

const environments = ['development', 'production', 'test'];
assert(
	environments.includes(config.env),
	`config.env must be one of "${environments.join('", "')}"`,
);

module.exports = {
	// isDev and isProd are convenience properties
	isDev: config.env === 'development',
	isProd: config.env === 'production',
	sessionSecret: config.sessionSecret,
	port: config.port,
};
