'use strict';

const Promise = require('bluebird');

// We separate out knex since it's broadly useful.
const knexFactory = require('knex');

// Models have their own separate builder
const makeModels = require('./models/index');

// Services will be collected and called iteratively
const makeAuthenticationService = require('./authentication/service');

// This is an ordered map, relying on the ES2015 change that objects are
// ordered.
// The property name will be the property on the eventual services object
// for the given service.
const serviceFactories = Object.entries({
	authentication: makeAuthenticationService,
});

// We treat all dependencies factories as if they are async, even though some
// or even all may not be.
module.exports = async function setupDependencies({ config }) {
	return Promise.try(() => {
		const knex = knexFactory(require('../knexfile'));
		return { config, knex };
	})
		.then(deps => {
			return Promise.try(() => {
				return makeModels(deps);
			}).then(models => {
				return { ...deps, models };
			});
		})
		.then(deps => {
			return Promise.reduce(
				serviceFactories,
				(services, [serviceName, factory]) => {
					return Promise.try(() => {
						return factory({ ...deps, services });
					}).then(service => {
						return { ...services, [serviceName]: service };
					});
				},
				{},
			).then(services => ({ ...deps, services }));
		});
};
