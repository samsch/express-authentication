'use strict';

module.exports = class FormError extends Error {
	constructor(errorsMap) {
		super('Form Errors');
		this.errorsMap = errorsMap;
	}
};
