'use strict';

const { getReasonPhrase } = require('http-status-codes');

module.exports = class HttpError extends Error {
	constructor(status = 500, message, prevError) {
		super(message ?? getReasonPhrase(status));
		this.status = status;
		this.previousError = prevError;
	}
};
