'use strict';

module.exports = class MessageError extends Error {
	constructor(message, status) {
		super(message);
		this.status = status ?? 500;
	}
};
