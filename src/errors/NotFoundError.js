'use strict';

module.exports = class NotFoundError extends Error {
	constructor(message = 'Not Found') {
		super(message);
	}
};
