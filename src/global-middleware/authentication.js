'use strict';

const { Router } = require('express');
const Promise = require('bluebird');
const NotFoundError = require('../errors/NotFoundError');

module.exports = function makeAuthenticationMiddleware({ models }) {
	const router = Router();

	router.use((req, res, next) => {
		if (req.session && req.session.user_id) {
			return Promise.try(() => {
				return models.user.findUserById(req.session.user_id);
			})
				.then(user => {
					req.user = user;
					next();
				})
				.catch(NotFoundError, () => {
					res.sessions.user_id = undefined;
					next();
				})
				.catch(error => {
					next(error);
				});
		}
		next();
		return undefined;
	});

	return router;
};
