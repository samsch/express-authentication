'use strict';

/**
 * CSRF protection stops cross origin requests from other websites which could
 * cause harm. While with modern browsers visiting a single-origin website,
 * the same attack should be blocked by sameSite lax or strict cookie setting,
 * it still is not ideal to rely on that always being the case. Instead, we
 * rely on the much older same-origin policy in browsers, which stops other
 * websites from being able to read the contents of responses -- including
 * cookies -- to our origin. A CSRF token is written to a cookie, and our
 * website reads that cookie and sends it with any requests which should cause
 * and update on the server. Generally, that's requests other than GET.
 *
 * csurf is the main library handling this, but we need to setup the cookie
 * as well as reading the token from headers or form data.
 *
 *  We use "XSRF-TOKEN" for the cookie name and "X-XSRF-TOKEN" for the header
 * name because these are common defaults, including in Axios. (If you use
 * Axios, this will automatically work, at least on the same origin.)
 *
 * csurf relies on express-session, as that's where it sets the token to
 * validate against.
 */

const { Router } = require('express');
const csrf = require('csurf');

module.exports = function useCSRFProtection({ config }) {
	const router = Router();

	router.use(
		csrf({
			value: req => {
				if (req.get('Content-Type') === 'application/x-www-form-urlencoded') {
					return req.body.csrf_token;
				}
				return req.get('X-XSRF-TOKEN');
			},
		}),
	);
	router.use(function (err, req, res, next) {
		if (err.code !== 'EBADCSRFTOKEN') {
			next(err);
			return;
		}
		if (config.isDev) {
			console.error('CSRF failure!');
		}
		res.set('X-XSRF-TOKEN', req.csrfToken());
		res.cookie('XSRF-TOKEN', req.csrfToken(), { sameSite: 'lax' });
		next(err);
	});

	function attachCSRFCookie(req, res) {
		res.cookie('XSRF-TOKEN', req.csrfToken(), { sameSite: 'lax' });
		req.session.csrfCookieSet = Date.now();
	}

	router.use(function (req, res, next) {
		req.attachCSRFCookie = attachCSRFCookie.bind(null, req, res);
		if (!req.session.csrfCookieSet) {
			req.attachCSRFCookie();
		}
		next();
	});

	return router;
};
