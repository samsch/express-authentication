'use strict';

/**
 * Server-side sessions are the basic mechanism use for authentication, as well
 * as holding other server-side information across requests.
 * The main tool is express-session: https://www.npmjs.com/package/express-session
 * The session connector passed as the `store` argument should be setup for your
 * primary database in most apps, which is what we are doing here, by using the
 * knex connection which is passed in, with connect-session-knex.
 *
 * Flash messages are a tool for setting data in the session which is removed when
 * it is read. The biggest usage is for html form handling, where you need to hold
 * onto form result information (such as success or error messages) after a page
 * redirect. This is not an important tool for security, and is safe to remove.
 */

const { Router } = require('express');
const session = require('express-session');
const makeFlash = require('connect-flash');
const KnexSessionConnector = require('connect-session-knex')(session);

module.exports = function makeSessionsMiddleware({ config, knex }) {
	const router = Router();

	router.use(
		// Be sure to check out the express-session docs for more information on these
		// options. `httpOnly` and `secure` must be true in production. If you are
		// doing cross origin requests with CORS that should be authenticated, then
		// sameSite will need to be set to `'none'`. Otherwise if you can, a single
		// origin site gains helpful extra protection by having sameSite set to lax
		// or strict.
		// With maxAge and expires unset, the cookies will be browser "session"
		// cookies, which will expire when the last tab for the site is closed.
		session({
			secret: config.sessionSecret,
			resave: false,
			saveUninitialized: false,
			rolling: false,
			store: new KnexSessionConnector({
				createtable: false,
				knex,
			}),
			cookie: {
				httpOnly: true,
				secure: config.isProd,
				sameSite: 'lax',
				// maxAge: 1000 * 60 * 60 * 24,
			},
		}),
	);

	router.use(makeFlash());

	return router;
};
