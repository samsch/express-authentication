'use strict';

const path = require('path');
const Promise = require('bluebird');
const express = require('express');

// Helmet provides fairly easy setup for various important security and privacy headers.
const helmet = require('helmet');

// This is a an Express render engine for Inferno and React
const createRenderer = require('@nsrv/express-jsx-views');

// ./config.js is how we access our application configuration, which holds any secrets.
const config = require('./config');

// There are common global middleware we need for various security and general
// functionality in the app.
const sessionsMiddleware = require('./global-middleware/sessions');
const csrfMiddleware = require('./global-middleware/csrf');
const authenticationMiddleware = require('./global-middleware/authentication');

const makeDependencies = require('./dependencies');

const makeRoutes = require('./routes');
const HttpError = require('./errors/HttpError');

Promise.try(() => {
	// This is where we setup application dependencies, such as models and services.
	// The returned deps include `{ config, knex, services, models }`
	return makeDependencies({ config });
})
	.then(deps => {
		const app = express();

		// non-middleware express configuration.
		app.set('views', path.resolve(__dirname, '../views'));
		app.set('view engine', 'jsx');
		app.engine('jsx', createRenderer('inferno'));

		if (config.isProd) {
			// In production we will have nginx or another server proxying to Node, and it will
			// handle the TLS certicate(s) for https. For Express to correctly work with a proxy,
			// you enable proxy trust which tells Express that the headers the proxy sets are
			// valid. The loopback options means we will expect the server to be on the same
			// server. IF your proxy is on a different server, you should either use a numeric
			// "hops" value, or the proxy's IP address. More information available here:
			// https://expressjs.com/en/guide/behind-proxies.html
			app.set('trust proxy', 'loopback');
		}

		if (config.isProd) {
			// We use Helmet defaults, which include some strict configs that are important in
			// production, but can create a lot of friction in development. Check out Helmets
			// docs (https://www.npmjs.com/package/helmet) to learn more.
			app.use(helmet());
		}

		if (!config.isProd) {
			// nginx will serve static files in production. If you want to always serve files with
			// express, you can remove the condition from around this.
			app.use(express.static(path.resolve(__dirname, 'static')));
		}

		// This is a small convenience middleware which makes it easy to access the request
		// object in rendered views (which are passed the `locals` object).
		app.use((req, res, next) => {
			res.locals.req = req;
			next();
		});

		// These two middlewares make it so that request bodies with url-encoded form data
		// or json bodies are translated into the `req.body` property. Information on this
		// can be found in the express docs here https://expressjs.com/en/4x/api.html#express
		// Also note that these do *not* parse multi-part form data. If you need to accept
		// file uploads, you will need to add another middleware for that.
		app.use(express.json());
		app.use(express.urlencoded({ extended: true }));

		[sessionsMiddleware, csrfMiddleware, authenticationMiddleware].forEach(
			makeMiddleware => {
				app.use(makeMiddleware(deps));
			},
		);

		app.use(makeRoutes(deps));

		// eslint-disable-next-line no-unused-vars
		app.use('*', (req, res) => {
			// eslint-disable-next-line no-console
			console.log(404, req.originalUrl);
			throw new HttpError(404);
		});

		// This bit of code lets us know which requests should get json vs html error responses.
		app.use('/api', (error, req, res, next) => {
			req.isApiRequest = true;
			next(error);
		});

		app.use((routerError, req, res, next) => {
			console.error('Dealing with an error');
			// If there is already a partial response, you must pass the error onto the default
			// Exrpess error handler.
			if (res.headersSent) {
				console.error(routerError);
				next(routerError);
				return;
			}
			Promise.reject(routerError)
				.catch({ code: 'EBADCSRFTOKEN' }, error => {
					throw new HttpError(403, 'Missing or invalid CSRF token', error);
				})
				.catch(
					error => {
						// Anything that isn't an HttpError already gets turned into one.
						return error instanceof HttpError === false;
					},
					error => {
						throw new HttpError(500, null, error);
					},
				)
				.catch(HttpError, error => {
					if (typeof error.status !== 'number') {
						console.error(error);
						throw new Error(
							'Why are you passing a non-number to the HttpError constructor for status?',
						);
					}
					// console.error(error);
					res.status(error.status);
					if (req.isApiRequest) {
						res.json({
							message: error.message,
						});
					} else {
						res.render('error-page.jsx', {
							status: error.status,
							reason: error.message,
						});
					}
				})
				.catch(error => {
					console.error('Error handling error');
					console.error(error);
					// If the error handling itself fails, we hand off to the built-in error handler.
					next(error);
				});
		});

		return app;
	})
	.then(app => {
		const port = config.port;
		const server = app.listen(port);
		server.on('listening', () => {
			// eslint-disable-next-line no-console
			console.log(`Server running at http://localhost:${port}`);
		});
		server.on('error', error => {
			if (error.code === 'EADDRINUSE') {
				console.error(
					`Something is already using port ${port}. You will need to find that other process and shut it down first.`,
				);
			}
			console.error('HTTP Server error', error);
		});
	})
	.catch(error => {
		console.error('Server startup failure', error);
	});
