'use strict';

const user = require('./user');

module.exports = function makeModels({ knex }) {
	const models = {};

	Object.entries({ user }).forEach(([name, makeModel]) => {
		models[name] = makeModel({ knex, models });
	});

	return models;
};
