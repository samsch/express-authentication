# Models

These "models" are really just collections of functions which have the shared state of access to other models.

Models mutate the `models` deps argument to add themselves to it. This allows each model to reference the others via that same object later.
