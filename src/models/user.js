'use strict';

const Promise = require('bluebird');
const MessageError = require('../errors/MessageError');
const throwIfEmpty = require('./util/throw-if-empty');

module.exports = function makeUserModel({ knex /* , models */ }) {
	async function findLoginUser(username) {
		return Promise.try(() => {
			return knex('users').where({ username }).first();
		}).then(throwIfEmpty);
	}
	async function findUserById(id) {
		return Promise.try(() => {
			return knex('users')
				.select(['id', 'username', 'name', 'created_at', 'updated_at'])
				.where({ id })
				.first();
		}).then(throwIfEmpty);
	}
	async function setPassword(id, hash) {
		return Promise.try(() => {
			return knex('users').update({ password: hash }, ['id']).where({ id });
		}).then(updatedRows => {
			if (updatedRows.length !== 1) {
				throw new MessageError('Failed to set password');
			}
		});
	}
	async function createUser(username, passwordHash, name) {
		return Promise.try(() => {
			return knex('users')
				.insert({
					username,
					password: passwordHash,
					name,
				})
				.returning(['id']);
		}).then(([{ id }]) => {
			return findUserById(id);
		});
	}
	// TODO update function

	// eslint-disable-next-line no-param-reassign
	return {
		findLoginUser,
		findUserById,
		setPassword,
		createUser,
	};
};
