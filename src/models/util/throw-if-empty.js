'use strict';

const NotFoundError = require('../../errors/NotFoundError');

module.exports = function throwIfEmpty(row) {
	if (row == null || (Array.isArray(row) && row.length === 0)) {
		throw new NotFoundError();
	}
	return row;
};
