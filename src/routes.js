'use strict';

const PromiseRouter = require('express-promise-router');
const authenticationRoutes = require('./authentication/routes');
const authenticationJsonRoutes = require('./authentication/json-routes');

module.exports = function makeRouter(deps) {
	const router = PromiseRouter();

	// These are the form handling login routes and profile page.
	// If you are building your login forms and profile page on the client only,
	// you can delete this.
	router.use('/user', authenticationRoutes(deps));

	// These are the plain json login and logout routes.
	// If you are only using html login forms, you can delete this.
	router.use('/api/authentication', authenticationJsonRoutes(deps));

	router.get('/', (req, res) => {
		res.render('index.jsx');
	});

	return router;
};
