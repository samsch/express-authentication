'use strict';

const CsrfField = require('./csrf-field');

const Action = ({ route, value = false, label }) => {
	if (!route || !label) {
		throw new Error(
			`route and label props required: { route, label }, received: ${JSON.stringify(
				{
					route,
					value,
					label,
				},
			)}`,
		);
	}
	return (
		<form action={route} method="post">
			<CsrfField />
			{value ? <input type="hidden" name="value" value={value} /> : null}
			<button type="submit">{label}</button>
		</form>
	);
};

module.exports = Action;
