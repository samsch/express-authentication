'use strict';

const Action = require('../action');

module.exports = function LogoutButton() {
	return <Action route="/user/logout" label="Log out" />;
};
