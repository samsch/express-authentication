'use strict';

const Html = require('../html');
const LogoutButton = require('./logout-button');

module.exports = function Profile({ _locals }) {
	const user = _locals.req.user;

	return (
		<Html title="Profile">
			<h1>User Profile</h1>
			<dl>
				<dt>Username</dt>
				<dd>{user.username}</dd>
				<dt>Preferred Name</dt>
				<dd>{user.name}</dd>
			</dl>
			<h2>Actions and Places</h2>
			<h3>Unauthenticated pages</h3>
			<ul>
				<li>
					<a href="/">Home</a>
				</li>
				<li>
					<a href="/user/register">Register</a>
				</li>
				<li>
					<a href="/user/login">Login</a>
				</li>
			</ul>
			<h3>Authenticated pages</h3>
			<ul>
				<li>
					<a href="/user/profile">User Profile (where we are now)</a>
				</li>
			</ul>
			<h3>Actions</h3>
			<ul>
				<li>
					<LogoutButton />
				</li>
			</ul>
		</Html>
	);
};
