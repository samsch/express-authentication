'use strict';

const CsrfField = require('../csrf-field');
const Html = require('../html');
const LogoutButton = require('./logout-button');

module.exports = function Login({ _locals }) {
	const { user } = _locals;
	const messages = _locals.req.flash('form-message');
	const flashlist = _locals.req.flash('form-errors');
	const errors = flashlist[flashlist.length - 1];
	return (
		<Html title="Login">
			<h1>Registration Page</h1>
			{user ? (
				<p>
					Already logged in as {user.name}. You can{' '}
					<a href="/user/profile">return to your profile</a> or <LogoutButton />
				</p>
			) : (
				<div>
					<h2>Registration Form</h2>
					<form method="post" action="">
						<CsrfField />
						<div>
							<label htmlFor="username">Username</label>
							<input type="text" id="username" name="username" />
							{errors?.username.map(message => (
								<p>{message}</p>
							))}
						</div>
						<div>
							<label htmlFor="password">Password</label>
							<input type="password" id="password" name="password" />
							{errors?.password.map(message => (
								<p>{message}</p>
							))}
						</div>
						<div>
							<label htmlFor="name">Name</label>
							<input
								type="text"
								id="name"
								name="name"
								aria-describedby="name-help-text"
							/>
							<p id="name-help-text">
								Your username will be used if you leave this blank
							</p>
							{errors?.name.map(message => (
								<p>{message}</p>
							))}
						</div>
						{messages.map(message => {
							return <p>{message}</p>;
						})}
						<div>
							<button type="submit">Create account and log in</button>
						</div>
					</form>
				</div>
			)}
		</Html>
	);
};
