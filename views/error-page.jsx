'use strict';

const Html = require('./html');

module.exports = function ErrorPage({ status, reason }) {
	return (
		<Html title={`Error ${status} - ${reason}`}>
			<h1>
				{status} - {reason}
			</h1>
			{status === 404 ? (
				<p>
					The page you tried to visit does not exist.{' '}
					<a href="/">Return to the home page?</a>
				</p>
			) : (
				<p>
					Whoops, this is embarrasing. Would you like to{' '}
					<a href="/">return to the home page?</a>
				</p>
			)}
		</Html>
	);
};
