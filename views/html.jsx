'use strict';

module.exports = function Html(
	// eslint-disable-next-line react/prop-types
	{ title, children, metaViewPort, metaCharset, headChildren },
	{ _locals },
) {
	if (!children) {
		throw new Error('DefaultLayout expects truthy `children` prop!');
	}
	if (!_locals) {
		throw new Error('DefaultLayout expects truthy `_locals` prop!');
	}
	// eslint-disable-next-line react/prop-types
	if (typeof title !== 'string' || title.length === 0) {
		throw new Error('DefaultLayout expects non-empty string `title` prop.');
	}
	const messages = _locals.req
		.flash('messages')
		.concat(_locals.req.flash('message'));
	const messageAlert = messages.length ? (
		<ul>
			{messages.map(message => {
				return <li>{message}</li>;
			})}
		</ul>
	) : null;
	return (
		<html lang="en">
			<head>
				{metaCharset ?? <meta charSet="UTF-8" />}
				{metaViewPort ?? (
					<meta
						name="viewport"
						content="width=device-width, initial-scale=1.0"
					/>
				)}
				<title>{title}</title>
				{headChildren}
			</head>
			<body>
				{messageAlert}
				<div>{children}</div>
			</body>
		</html>
	);
};
