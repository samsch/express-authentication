'use strict';

const Html = require('./html');
const LogoutButton = require('./authentication/logout-button');

module.exports = function Home({ _locals }) {
	const user = _locals.req.user;

	return (
		<Html title="Home">
			<h1>Home</h1>
			<p>
				This is an example app. It runs through the basics of authentication
				with Express.
			</p>
			<h2>User Info</h2>
			{user ? (
				<dl>
					<dt>Username</dt>
					<dd>{user.username}</dd>
					<dt>Preferred Name</dt>
					<dd>{user.name}</dd>
				</dl>
			) : (
				<p>Not currently logged in.</p>
			)}
			<h2>Actions and Places</h2>
			<h3>Unauthenticated pages</h3>
			<ul>
				<li>
					<a href="/">Home (where we are now)</a>
				</li>
				<li>
					<a href="/user/register">Register</a>
				</li>
				<li>
					<a href="/user/login">Login</a>
				</li>
			</ul>
			<h3>Authenticated pages</h3>
			<ul>
				<li>
					<a href="/user/profile">User Profile</a>
				</li>
			</ul>
			<h3>Actions</h3>
			<ul>
				<li>
					<LogoutButton />
				</li>
			</ul>
		</Html>
	);
};
